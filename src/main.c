#include <windows.h>
#include <shellapi.h>

#include "globaldefines.h"

//==============================================================================

int main()
{
	// Initialize global variables
	g_draggingMainWindow = 0;

	g_drawTextParams.cbSize = sizeof(DRAWTEXTPARAMS);
	g_drawTextParams.iLeftMargin = DEFAULT_TEXT_MARGIN_CHARS;
	g_drawTextParams.iRightMargin = DEFAULT_TEXT_MARGIN_CHARS;
	g_drawTextParams.iTabLength = DEFAULT_TAB_LENGTH_CHARS;

	RECT desktopRect;
	HWND hDesktopWindow = GetDesktopWindow();
	GetWindowRect(hDesktopWindow, &desktopRect);
	g_mainWindowPopUpMenuTrackParams.cbSize = sizeof(TPMPARAMS);
	g_mainWindowPopUpMenuTrackParams.rcExclude = desktopRect;

	g_hMainWindowPopUpMenu = CreatePopupMenu();
	AppendMenuW(g_hMainWindowPopUpMenu, MF_STRING, IDM_STAY_ON_TOP,
		L"Stay on top");
	AppendMenuW(g_hMainWindowPopUpMenu, MF_STRING, IDM_SETTINGS, L"Settings");
	AppendMenuW(g_hMainWindowPopUpMenu, MF_STRING, IDM_ABOUT, L"About");
	AppendMenuW(g_hMainWindowPopUpMenu, MF_STRING, IDM_QUIT, L"Quit");

	g_hNotifyIconPopUpMenu = CreatePopupMenu();
	AppendMenuW(g_hNotifyIconPopUpMenu, MF_STRING,
		IDM_CENTER_WINDOW_ON_SCREEN, L"Center window");
	AppendMenuW(g_hNotifyIconPopUpMenu, MF_STRING, IDM_STAY_ON_TOP,
		L"Stay on top");
	AppendMenuW(g_hNotifyIconPopUpMenu, MF_STRING, IDM_SETTINGS, L"Settings");
	AppendMenuW(g_hNotifyIconPopUpMenu, MF_STRING, IDM_ABOUT, L"About");
	AppendMenuW(g_hNotifyIconPopUpMenu, MF_STRING, IDM_QUIT, L"Quit");

	g_hInstance = GetModuleHandleW(0);

	g_hApplicationIcon = (HICON)LoadImage(g_hInstance,
		MAKEINTRESOURCE(MAIN_ICON),
		IMAGE_ICON, 0, 0, LR_DEFAULTSIZE);

	g_hApplicationSmallIcon = (HICON)LoadImage(g_hInstance,
		MAKEINTRESOURCE(MAIN_ICON),
		IMAGE_ICON, GetSystemMetrics(SM_CXSMICON),
		GetSystemMetrics(SM_CYSMICON), 0);

	const wchar_t MAIN_WINDOW_CLASS_NAME[] = L"MCMainWindow";

	// Register window class
	WNDCLASSEXW MainWindowClass;
	MainWindowClass.cbSize = sizeof(WNDCLASSEXW);
	MainWindowClass.style = 0;
	MainWindowClass.lpfnWndProc = (WNDPROC)&MainProc;
	MainWindowClass.cbClsExtra = 0;
	MainWindowClass.cbWndExtra = 0;
	MainWindowClass.hInstance = g_hInstance;
	MainWindowClass.hIcon = g_hApplicationIcon;
	MainWindowClass.hCursor = LoadCursor(0, IDC_ARROW);
	MainWindowClass.hbrBackground = (HBRUSH)COLOR_WINDOW;
	MainWindowClass.lpszMenuName = 0;
	MainWindowClass.lpszClassName = (LPWSTR)MAIN_WINDOW_CLASS_NAME;
	MainWindowClass.hIconSm = g_hApplicationSmallIcon;
	MainWindowClass.hbrBackground = NULL;

	RegisterClassExW(&MainWindowClass);

	int windowX = (desktopRect.right - desktopRect.left -
		DEFAULT_CLOCK_WINDOW_WIDTH) / 2;
	int windowY = (desktopRect.bottom - desktopRect.top -
		DEFAULT_CLOCK_WINDOW_HEIGHT) / 2;

	g_hMainWindow = CreateWindowExW(
		0 // Extended window style
		| WS_EX_TOOLWINDOW
		,
		(LPWSTR)MAIN_WINDOW_CLASS_NAME,
		(LPWSTR)APPLICATION_NAME,
		0 // Window style
		| WS_SIZEBOX
		| WS_POPUP
		,
		windowX, windowY,
		DEFAULT_CLOCK_WINDOW_WIDTH, DEFAULT_CLOCK_WINDOW_HEIGHT,
		0, 0, g_hInstance, 0);

	// Create notify icon
	g_notifyIconData.cbSize = NOTIFYICONDATAW_V1_SIZE;
	g_notifyIconData.hWnd = g_hMainWindow;
	g_notifyIconData.uID = IDC_NOTIFY_ICON;
	g_notifyIconData.uFlags = 0
		| NIF_MESSAGE
		| NIF_ICON
		| NIF_TIP
		;
	g_notifyIconData.uCallbackMessage = WM_NOTYFYICON;
	g_notifyIconData.hIcon = g_hApplicationSmallIcon;
	wcsncpy(g_notifyIconData.szTip, APPLICATION_NAME, 64);

	Shell_NotifyIcon(NIM_ADD, &g_notifyIconData);

	loadSettings();
	// Fix if settings were incorrect.
	saveSettings();

	// Show window, handle messages
	ShowWindow(g_hMainWindow, SW_SHOW);
	paintMainWindow();

	SetTimer(g_hMainWindow, 0, 500, NULL);

	MSG msg;

	while(1)
	{
		GetMessageW(&msg, 0, 0, 0);
		if(msg.message == WM_QUIT)
		{
			break;
		}
		else
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return((int)msg.wParam);
}

// END OF int main()
//==============================================================================
