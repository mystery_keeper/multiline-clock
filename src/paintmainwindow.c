#include <assert.h>
#include <string.h>

#include "globaldefines.h"

void paintMainWindow()
{
	RECT MainWindowClientRect;
	GetClientRect(g_hMainWindow, &MainWindowClientRect);

	RECT DrawBufferRect = {0, 0,
		MainWindowClientRect.right - MainWindowClientRect.left,
		MainWindowClientRect.bottom - MainWindowClientRect.top};

	HDC hMainWindowDC = GetDC(g_hMainWindow);
	HDC hDrawBufferDC = CreateCompatibleDC(hMainWindowDC);

	HBITMAP hBufferBitmap = CreateCompatibleBitmap(hMainWindowDC,
		DrawBufferRect.right, DrawBufferRect.bottom);
	HBITMAP hBufferBitmapOld = SelectObject(hDrawBufferDC, hBufferBitmap);

	// Calculating text metrics.
	int textFullHeight = 0;
	int linesCount = 0;

	ClockLine * pClockLine = NULL;
	for(pClockLine = g_pFirstClockLine; pClockLine != NULL;
		pClockLine = pClockLine->pNext)
	{
		linesCount++;

		HFONT hNewFont = CreateFontIndirectW(&pClockLine->data.fontDescriptor);
		HFONT hOldFont = (HFONT)SelectObject(hDrawBufferDC, hNewFont);
		TEXTMETRIC metric;
		GetTextMetrics(hDrawBufferDC, &metric);
		SelectObject(hDrawBufferDC, hOldFont);
		DeleteObject(hNewFont);
		pClockLine->height = metric.tmHeight;

		textFullHeight += pClockLine->height;
		if(pClockLine != g_pFirstClockLine)
			textFullHeight += DEFAULT_SPACE_BETWEEN_LINES;
	}

	int voffsetFirst = (MainWindowClientRect.bottom - MainWindowClientRect.top -
		textFullHeight) / 2;

	int voffset = voffsetFirst;
	for(pClockLine = g_pFirstClockLine; pClockLine != NULL;
		pClockLine = pClockLine->pNext)
	{
		pClockLine->voffset = voffset;
		voffset += pClockLine->height + DEFAULT_SPACE_BETWEEN_LINES;
	}

	// Filling background.
	HBRUSH hBackgroundBrush = CreateSolidBrush(
		RGB(g_commonSettings.backgroundRed,
		g_commonSettings.backgroundGreen, g_commonSettings.backgroundBlue));
	FillRect(hDrawBufferDC, &DrawBufferRect, hBackgroundBrush);
	SetBkMode(hDrawBufferDC, TRANSPARENT);

	// Drawing text lines.
	for(pClockLine = g_pFirstClockLine; pClockLine != NULL;
		pClockLine = pClockLine->pNext)
	{
		SetTextColor(hDrawBufferDC, RGB(pClockLine->data.colorRed,
			pClockLine->data.colorGreen, pClockLine->data.colorBlue));

		HFONT hNewFont = CreateFontIndirectW(&pClockLine->data.fontDescriptor);
		HFONT hOldFont = (HFONT)SelectObject(hDrawBufferDC, hNewFont);

		RECT TextBoundingRect;
		memcpy(&TextBoundingRect, &DrawBufferRect, sizeof(RECT));
		TextBoundingRect.top = pClockLine->voffset;
		TextBoundingRect.bottom = pClockLine->voffset + pClockLine->height;

		wchar_t timeString[1025] = {0};
		formTimeDateString(pClockLine->data.format, &timeString[0], 1024);

		DrawTextExW(hDrawBufferDC,
			(LPWSTR)timeString,
			-1, &TextBoundingRect,
			0
			| DT_CENTER
			| DT_SINGLELINE
			,
			&g_drawTextParams);

		SelectObject(hDrawBufferDC, hOldFont);
		DeleteObject(hNewFont);
	}

	// Painting margins
	RECT marginRect = {0, 0, DrawBufferRect.right, DrawBufferRect.top +
		DEFAULT_TEXT_MARGIN_PIXELS};
	FillRect(hDrawBufferDC, &marginRect, hBackgroundBrush);
	marginRect.top = DrawBufferRect.bottom - DEFAULT_TEXT_MARGIN_PIXELS;
	marginRect.bottom = DrawBufferRect.bottom;
	FillRect(hDrawBufferDC, &marginRect, hBackgroundBrush);
	marginRect.left = 0;
	marginRect.right = DEFAULT_TEXT_MARGIN_PIXELS;
	marginRect.top = 0;
	marginRect.bottom = DrawBufferRect.bottom;
	FillRect(hDrawBufferDC, &marginRect, hBackgroundBrush);
	marginRect.left = DrawBufferRect.right - DEFAULT_TEXT_MARGIN_PIXELS;
	marginRect.right = DrawBufferRect.right;
	FillRect(hDrawBufferDC, &marginRect, hBackgroundBrush);

	DeleteObject(hBackgroundBrush);

	BitBlt(hMainWindowDC,
		MainWindowClientRect.left, MainWindowClientRect.top,
		DrawBufferRect.right, DrawBufferRect.bottom,
		hDrawBufferDC,
		0, 0,
		SRCCOPY);

	SelectObject(hDrawBufferDC, hBufferBitmapOld);
	DeleteObject(hBufferBitmap);
	DeleteDC(hDrawBufferDC);
	ReleaseDC(g_hMainWindow, hMainWindowDC);
	ValidateRect(g_hMainWindow, NULL);
}
