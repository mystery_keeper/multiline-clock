#include "globaldefines.h"

//==============================================================================
// Constants:

const wchar_t APPLICATION_NAME[] = L"Multiline Clock";

const UINT_PTR IDM_CENTER_WINDOW_ON_SCREEN = 10000u;
const UINT_PTR IDM_STAY_ON_TOP = 10003u;
const UINT_PTR IDM_SETTINGS = 10001u;
const UINT_PTR IDM_ABOUT = 10002u;
const UINT_PTR IDM_QUIT = 11000u;

const UINT IDC_NOTIFY_ICON = 2u;
const UINT WM_NOTYFYICON = WM_USER + 1u;

const int DEFAULT_CLOCK_WINDOW_WIDTH = 200;
const int DEFAULT_CLOCK_WINDOW_HEIGHT = 110;
const wchar_t DEFAULT_LINE_FORMAT[] = L"%HH:%mm:%ss";
const unsigned int DEFAULT_TEXT_MARGIN_PIXELS = 2u;
const unsigned int DEFAULT_TEXT_MARGIN_CHARS = 0u;
const unsigned int DEFAULT_SPACE_BETWEEN_LINES = 0u;
const int DEFAULT_TEXT_SIZE_IN_POINTS = -28;
const unsigned int DEFAULT_TAB_LENGTH_CHARS = 4u;
const unsigned char DEFAULT_TEXT_COLOR_R = 0u;
const unsigned char DEFAULT_TEXT_COLOR_G = 0u;
const unsigned char DEFAULT_TEXT_COLOR_B = 0u;
const unsigned char DEFAULT_BACKGROUND_COLOR_R = 255u;
const unsigned char DEFAULT_BACKGROUND_COLOR_G = 255u;
const unsigned char DEFAULT_BACKGROUND_COLOR_B = 224u;
const BOOL DEFAULT_STAY_ON_TOP = FALSE;

const uint32_t CONFIG_FILE_VERSION = 2u;
const wchar_t * CONFIG_FILE_NAME = L"multilineclock.cfg";

// END OF Constants
//==============================================================================
// Variables:

HINSTANCE g_hInstance = 0;

HWND g_hMainWindow = 0;

DRAWTEXTPARAMS g_drawTextParams;

int g_draggingMainWindow = 0;

POINT g_mousePosition;

HMENU g_hMainWindowPopUpMenu;
TPMPARAMS g_mainWindowPopUpMenuTrackParams;

HICON g_hApplicationIcon;
HICON g_hApplicationSmallIcon;

NOTIFYICONDATA g_notifyIconData;
HMENU g_hNotifyIconPopUpMenu;

ClockLine * g_pFirstClockLine = NULL;

CommonSettings g_commonSettings;

COLORREF g_customColors[CUSTOM_COLORS_NUMBER];

HBRUSH g_hBackgroundBrush = 0;
HBRUSH g_hCurrentLineBrush = 0;

int g_modalWindowIsOpened = 0;

// END OF Variables
//==============================================================================
