#include <windowsx.h>
#include <wchar.h>

#include "globaldefines.h"

#define ABOUT_STRING_SIZE 1024
#define ABOUT_CAPTION_SIZE 256

LRESULT APIENTRY MainProc(HWND a_hWnd, UINT a_message, WPARAM a_wParam,
	LPARAM a_lParam)
{
	if(a_message == WM_DESTROY)
	{
		saveSettings();
		cleanUpAndQuit();
	}
	else if(a_message == WM_PAINT)
	{
		paintMainWindow();
	}
	else if(a_message == WM_TIMER)
	{
		InvalidateRect(g_hMainWindow, NULL, FALSE);
	}
	else if(a_message == WM_SIZE)
	{
		InvalidateRect(g_hMainWindow, NULL, FALSE);
	}
	else if(a_message == WM_ERASEBKGND)
	{
		return (LRESULT)1;
	}
	else if(a_message == WM_LBUTTONDOWN && a_hWnd == g_hMainWindow)
	{
		RECT MainWindowClientRect;
		GetClientRect(g_hMainWindow, &MainWindowClientRect);
		g_mousePosition.x = GET_X_LPARAM(a_lParam);
		g_mousePosition.y = GET_Y_LPARAM(a_lParam);
		g_draggingMainWindow = 1;
		SetCapture(g_hMainWindow);
	}
	else if(a_message == WM_LBUTTONUP && a_hWnd == g_hMainWindow)
	{
		g_draggingMainWindow = 0;
		ReleaseCapture();
		saveSettings();
	}
	else if(a_message == WM_MOUSEMOVE && a_hWnd == g_hMainWindow
		&& g_draggingMainWindow)
	{
		POINT MousePositionShift;
		MousePositionShift.x = GET_X_LPARAM(a_lParam) - g_mousePosition.x;
		MousePositionShift.y = GET_Y_LPARAM(a_lParam) - g_mousePosition.y;

		RECT MainWindowRect;
		GetWindowRect(g_hMainWindow, &MainWindowRect);

		SetWindowPos(g_hMainWindow, NULL,
			MainWindowRect.left + MousePositionShift.x,
			MainWindowRect.top + MousePositionShift.y,
			0, 0,
			SWP_NOCOPYBITS | SWP_NOSIZE);
	}
	else if((a_message == WM_RBUTTONUP || a_message == WM_CONTEXTMENU)
		&& a_hWnd == g_hMainWindow)
	{
		POINT RightClickMousePosition =
			{GET_X_LPARAM(a_lParam), GET_Y_LPARAM(a_lParam)};
		ClientToScreen(g_hMainWindow, &RightClickMousePosition);
		TrackPopupMenuEx(g_hMainWindowPopUpMenu,
			TPM_RIGHTBUTTON | TPM_NOANIMATION,
			RightClickMousePosition.x, RightClickMousePosition.y,
			g_hMainWindow, &g_mainWindowPopUpMenuTrackParams);
	}
	else if((a_message == WM_COMMAND) && (a_hWnd == g_hMainWindow) &&
		(HIWORD(a_wParam) == 0))
	{
        if(LOWORD(a_wParam == IDM_CENTER_WINDOW_ON_SCREEN))
		{
			RECT DesktopRect, MainWindowRect;
			HWND hDesktopWindow = GetDesktopWindow();
			GetWindowRect(hDesktopWindow, &DesktopRect);
			GetWindowRect(g_hMainWindow, &MainWindowRect);
			int x = (DesktopRect.right - DesktopRect.left) / 2 -
				(MainWindowRect.right - MainWindowRect.left) / 2;
			int y = (DesktopRect.bottom - DesktopRect.top) / 2 -
				(MainWindowRect.bottom - MainWindowRect.top) / 2;
			SetWindowPos(g_hMainWindow, NULL, x, y, 0, 0,
				SWP_NOCOPYBITS | SWP_NOSIZE);
		}
		else if(LOWORD(a_wParam == IDM_STAY_ON_TOP))
		{
			setStayOnTop(!g_commonSettings.stayOnTop);
			saveSettings();
		}
		else if(LOWORD(a_wParam == IDM_SETTINGS))
		{
			if(g_modalWindowIsOpened)
			{
				return FALSE;
			}

            INT_PTR result = DialogBoxW(g_hInstance,
				MAKEINTRESOURCE(IDD_SETTINGS_DIALOG),
				g_hMainWindow, (DLGPROC)&SettingsDialogProc);

			(void)result;
		}
		else if(LOWORD(a_wParam == IDM_ABOUT))
		{
            wchar_t * aboutStringFormat = L"%ls by Aleksey "
				L"[Mystery Keeper] Lyashin\n\n"
				L"This software is absolutely free.\n"
				L"Do whatever you want with it."
				;

			wchar_t * aboutCaptionFormat = L"About %ls";

			wchar_t aboutString[ABOUT_STRING_SIZE] = {0};
			swprintf(aboutString, ABOUT_STRING_SIZE, aboutStringFormat,
				APPLICATION_NAME);

			wchar_t aboutCaption[ABOUT_STRING_SIZE] = {0};
			swprintf(aboutCaption, ABOUT_CAPTION_SIZE, aboutCaptionFormat,
				APPLICATION_NAME);

			MessageBoxW(g_hMainWindow, aboutString, aboutCaption,
				MB_ICONINFORMATION);
		}
		else if(LOWORD(a_wParam == IDM_QUIT))
		{
			saveSettings();
			cleanUpAndQuit();
		}
	}
	else if(a_message == WM_NOTYFYICON)
	{
		if(a_lParam == WM_LBUTTONDBLCLK)
		{
			SetForegroundWindow(g_hMainWindow);
		}
		else if(a_lParam == WM_RBUTTONUP || a_lParam == WM_CONTEXTMENU)
		{
			SetForegroundWindow(a_hWnd);
			POINT RightClickMousePosition;
			GetCursorPos(&RightClickMousePosition);
			TrackPopupMenuEx(g_hNotifyIconPopUpMenu,
				TPM_RIGHTBUTTON | TPM_NOANIMATION | TPM_BOTTOMALIGN,
				RightClickMousePosition.x, RightClickMousePosition.y,
				g_hMainWindow, &g_mainWindowPopUpMenuTrackParams);
		}
	}
	else
	{
		return DefWindowProcW(a_hWnd, a_message, a_wParam, a_lParam);
	}

	return 0;
}
