#include "globaldefines.h"

#include <windows.h>
#include <shlobj.h>
#include <stdio.h>
#include <assert.h>

//==============================================================================

void cleanUpAndQuit()
{
	clearClockLines(&g_pFirstClockLine);
	Shell_NotifyIcon(NIM_DELETE, &g_notifyIconData);
	DestroyMenu(g_hMainWindowPopUpMenu);
	DestroyMenu(g_hNotifyIconPopUpMenu);
	PostQuitMessage(0);
}

// END OF void CleanUpAndQuit()
//==============================================================================

void clearClockLines(ClockLine ** a_ppFirstLine)
{
	ClockLine * pCurrentLine = *a_ppFirstLine;
	ClockLine * pNextLine = NULL;

	while(pCurrentLine != NULL)
	{
		pNextLine = pCurrentLine->pNext;
		free(pCurrentLine);
		pCurrentLine = pNextLine;
	}

	*a_ppFirstLine = NULL;
}

// END OF void clearClockLines()
//==============================================================================

void copyClockLines(ClockLine ** a_ppDestinationFirstLine,
	ClockLine * a_pSourceFirstLine)
{
	ClockLine * pCurrentLine = NULL;
	ClockLine * pPreviousLine = NULL;

	if(*a_ppDestinationFirstLine != NULL)
		clearClockLines(a_ppDestinationFirstLine);

	for(ClockLine * pCurrentSourceLine = a_pSourceFirstLine;
		pCurrentSourceLine != NULL;
		pCurrentSourceLine = pCurrentSourceLine->pNext)
	{
		pCurrentLine = (ClockLine *)malloc(sizeof(ClockLine));
		memcpy(pCurrentLine, pCurrentSourceLine, sizeof(ClockLine));
		if(pPreviousLine != NULL)
			pPreviousLine->pNext = pCurrentLine;
		pPreviousLine = pCurrentLine;

		if(*a_ppDestinationFirstLine == NULL)
			*a_ppDestinationFirstLine = pCurrentLine;
	}
}

// END OF void copyClockLines(ClockLine ** a_ppDestinationFirstLine,
//		ClockLine * a_pSourceFirstLine)
//==============================================================================

void setClockLineToDefault(ClockLine * a_pClockLine)
{
	ZeroMemory(a_pClockLine, sizeof(ClockLine));

	wcscpy(a_pClockLine->data.format, DEFAULT_LINE_FORMAT);

	a_pClockLine->data.fontDescriptor.lfHeight = DEFAULT_TEXT_SIZE_IN_POINTS;
	a_pClockLine->data.fontDescriptor.lfWidth = 0;
	a_pClockLine->data.fontDescriptor.lfEscapement = 0;
	a_pClockLine->data.fontDescriptor.lfOrientation = 0;
	a_pClockLine->data.fontDescriptor.lfWeight = FW_DONTCARE;
	a_pClockLine->data.fontDescriptor.lfItalic = 0;
	a_pClockLine->data.fontDescriptor.lfUnderline = 0;
	a_pClockLine->data.fontDescriptor.lfStrikeOut = 0;
	a_pClockLine->data.fontDescriptor.lfCharSet = DEFAULT_CHARSET;
	a_pClockLine->data.fontDescriptor.lfOutPrecision = OUT_TT_ONLY_PRECIS;
	a_pClockLine->data.fontDescriptor.lfClipPrecision = CLIP_DEFAULT_PRECIS;
	a_pClockLine->data.fontDescriptor.lfQuality = CLEARTYPE_QUALITY;
	a_pClockLine->data.fontDescriptor.lfPitchAndFamily =
		DEFAULT_PITCH | FF_DONTCARE;
	wcscpy(a_pClockLine->data.fontDescriptor.lfFaceName, L"Arial");

	a_pClockLine->data.colorRed = DEFAULT_TEXT_COLOR_R;
	a_pClockLine->data.colorGreen = DEFAULT_TEXT_COLOR_G;
	a_pClockLine->data.colorBlue = DEFAULT_TEXT_COLOR_B;
}

// END OF void setClockLineToDefault(ClockLine * a_pClockLine)
//==============================================================================

void setStayOnTop(BOOL a_stayOnTop)
{
	g_commonSettings.stayOnTop = a_stayOnTop;

	HWND hWndAfter = HWND_NOTOPMOST;
	UINT uCheck = MF_BYCOMMAND | MF_UNCHECKED;

	if(g_commonSettings.stayOnTop)
	{
		hWndAfter = HWND_TOPMOST;
		uCheck = MF_BYCOMMAND | MF_CHECKED;
	}

	SetWindowPos(g_hMainWindow, hWndAfter, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);
	CheckMenuItem(g_hMainWindowPopUpMenu, IDM_STAY_ON_TOP, uCheck);
	CheckMenuItem(g_hNotifyIconPopUpMenu, IDM_STAY_ON_TOP, uCheck);
}

// END OF void setStayOnTop(BOOL a_stayOnTop)
//==============================================================================
