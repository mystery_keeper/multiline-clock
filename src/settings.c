#include <shlobj.h>
#include <stdio.h>
#include <assert.h>
#include <commctrl.h>

#include "globaldefines.h"

#define SETTINGS_WINDOW_CAPTION_SIZE 256
#define LINE_FORMAT_HELP_STRING_SIZE 1024

void saveSettings()
{
	WINDOWPLACEMENT mainWindowPlacement;
	mainWindowPlacement.length = sizeof(WINDOWPLACEMENT);
	GetWindowPlacement(g_hMainWindow, &mainWindowPlacement);

	wchar_t configFilePath[MAX_PATH];
	SHGetFolderPathW(NULL, CSIDL_APPDATA, NULL, SHGFP_TYPE_CURRENT,
		(LPWSTR)&configFilePath);
	wcsncat(configFilePath, L"\\", MAX_PATH - wcslen(configFilePath));
	wcsncat(configFilePath, CONFIG_FILE_NAME,
		MAX_PATH - wcslen(configFilePath));
	FILE * hConfigFile = _wfopen(configFilePath, L"wb");
	if(hConfigFile == NULL)
	{
		const size_t errorMessageSize = MAX_PATH + 50;
		wchar_t errorMessage[errorMessageSize];
		memset(errorMessage, 0, sizeof(wchar_t) * errorMessageSize);
		wcsncat(errorMessage,  L"Failed to open file ",
			errorMessageSize - wcslen(errorMessage));
		wcsncat(errorMessage, configFilePath,
			errorMessageSize - wcslen(errorMessage));
		wcsncat(errorMessage, L" for writing.",
			errorMessageSize - wcslen(errorMessage));
		MessageBoxW(g_hMainWindow, errorMessage, L"Error writing settings!",
			MB_ICONERROR);
		return;
	}

	fwrite(&CONFIG_FILE_VERSION, sizeof(CONFIG_FILE_VERSION), 1, hConfigFile);
	fwrite(&g_commonSettings.backgroundRed,
		sizeof(g_commonSettings.backgroundRed), 1, hConfigFile);
	fwrite(&g_commonSettings.backgroundGreen,
		sizeof(g_commonSettings.backgroundGreen), 1, hConfigFile);
	fwrite(&g_commonSettings.backgroundBlue,
		sizeof(g_commonSettings.backgroundBlue), 1, hConfigFile);
	fwrite(&g_commonSettings.stayOnTop,
		sizeof(g_commonSettings.stayOnTop), 1, hConfigFile);
	fwrite(&mainWindowPlacement, mainWindowPlacement.length, 1, hConfigFile);

	ClockLine * pCurrentLine = NULL;
	for(pCurrentLine = g_pFirstClockLine; pCurrentLine != NULL;
		pCurrentLine = pCurrentLine->pNext)
	{
		fwrite(&pCurrentLine->data, sizeof(ClockLineData), 1, hConfigFile);
	}

	fclose(hConfigFile);
}

// END OF void saveSettings()
//==============================================================================

void createDefaultClockLines()
{
	ClockLine * pNewLine = (ClockLine *)malloc(sizeof(ClockLine));
	setClockLineToDefault(pNewLine);

	g_pFirstClockLine = pNewLine;

	pNewLine->pNext = (ClockLine *)malloc(sizeof(ClockLine));
	pNewLine = pNewLine->pNext;
	setClockLineToDefault(pNewLine);
	wcscpy(pNewLine->data.format, L"%dddd");

	pNewLine->pNext = (ClockLine *)malloc(sizeof(ClockLine));
	pNewLine = pNewLine->pNext;
	setClockLineToDefault(pNewLine);
	wcscpy(pNewLine->data.format, L"%dd.%MM.%yyyy");
}

// END OF void createDefaultClockLines()
//==============================================================================

void loadSettings()
{
	g_commonSettings.backgroundRed = DEFAULT_BACKGROUND_COLOR_R;
	g_commonSettings.backgroundGreen = DEFAULT_BACKGROUND_COLOR_G;
	g_commonSettings.backgroundBlue = DEFAULT_BACKGROUND_COLOR_B;
	g_commonSettings.stayOnTop = DEFAULT_STAY_ON_TOP;

	g_customColors[0] = RGB(DEFAULT_BACKGROUND_COLOR_R,
		DEFAULT_BACKGROUND_COLOR_G, DEFAULT_BACKGROUND_COLOR_B);

	wchar_t configFilePath[MAX_PATH];
	SHGetFolderPathW(NULL, CSIDL_APPDATA, NULL, SHGFP_TYPE_CURRENT,
		(LPWSTR)&configFilePath);
	wcsncat(configFilePath, L"\\", MAX_PATH - wcslen(configFilePath));
	wcsncat(configFilePath, CONFIG_FILE_NAME,
		MAX_PATH - wcslen(configFilePath));
	FILE * hConfigFile = _wfopen(configFilePath, L"rb");
	if(hConfigFile == NULL)
	{
		const size_t errorMessageSize = MAX_PATH + 50;
		wchar_t errorMessage[errorMessageSize];
		memset(errorMessage, 0, sizeof(wchar_t) * errorMessageSize);
		wcsncat(errorMessage, L"Failed to open file ",
			errorMessageSize - wcslen(errorMessage));
		wcsncat(errorMessage, configFilePath,
			errorMessageSize - wcslen(errorMessage));
		wcsncat(errorMessage, L".\nDefault settings will be used.",
			errorMessageSize - wcslen(errorMessage));
		MessageBoxW(g_hMainWindow, errorMessage, L"Error reading settings!",
			MB_ICONINFORMATION);

		createDefaultClockLines();

		ClockLine * pClockLine;
		size_t i;
		for(pClockLine = g_pFirstClockLine, i = 1u;
			(pClockLine != NULL) && (i < CUSTOM_COLORS_NUMBER);
			pClockLine = pClockLine->pNext, i++)
		{
			g_customColors[i] = RGB(pClockLine->data.colorRed,
				pClockLine->data.colorGreen, pClockLine->data.colorBlue);
		}

		return;
	}

	fseek(hConfigFile, 0, SEEK_END);
	size_t configFileSize = ftell(hConfigFile);
	fseek(hConfigFile, 0, SEEK_SET);

	uint8_t * pConfigData = (uint8_t *)malloc(configFileSize);
	size_t bytesRead = fread(pConfigData, 1u, configFileSize, hConfigFile);
	assert(bytesRead == configFileSize);
	fclose(hConfigFile);

	int linesLoaded = 0;
	ClockLine * pNewFirstLine = NULL;
	ClockLine * pNewLastLine = NULL;

	// Using cycle for safer error control.
	// Break goes to cleanup and return.
	do
	{
		bytesRead = 0;
		const uint8_t * pData = pConfigData;

		uint32_t configFileVersion;
		if(configFileSize < sizeof(configFileVersion))
			break;
		configFileVersion = *(uint32_t *)pData;
		bytesRead += sizeof(configFileVersion);
		pData += sizeof(configFileVersion);

		if(configFileSize - bytesRead < sizeof(g_commonSettings.backgroundRed))
			break;
		g_commonSettings.backgroundRed = *pData;
		pData += sizeof(g_commonSettings.backgroundRed);

		if(configFileSize - bytesRead <
			sizeof(g_commonSettings.backgroundGreen))
			break;
		g_commonSettings.backgroundGreen = *pData;
		pData += sizeof(g_commonSettings.backgroundGreen);

		if(configFileSize - bytesRead < sizeof(g_commonSettings.backgroundBlue))
			break;
		g_commonSettings.backgroundBlue = *pData;
		pData += sizeof(g_commonSettings.backgroundBlue);

		if(configFileVersion >= 2u)
		{
			if(configFileSize - bytesRead < sizeof(g_commonSettings.stayOnTop))
				break;
			g_commonSettings.stayOnTop = *(BOOL *)pData;
			pData += sizeof(g_commonSettings.stayOnTop);
		}

		if(configFileSize - bytesRead < sizeof(UINT))
			break;
		UINT windowPlacementStructLength = *(UINT *)pData;
		if(configFileSize - bytesRead < windowPlacementStructLength)
			break;

		SetWindowPlacement(g_hMainWindow, (WINDOWPLACEMENT *)pData);
		bytesRead += windowPlacementStructLength;
		pData += windowPlacementStructLength;

		setStayOnTop(g_commonSettings.stayOnTop);

		while(configFileSize - bytesRead >= sizeof(ClockLineData))
		{
			ClockLine * pNewLine = (ClockLine *)malloc(sizeof(ClockLine));
			ZeroMemory(pNewLine, sizeof(ClockLine));

			memcpy(&pNewLine->data, pData, sizeof(ClockLineData));
			bytesRead += sizeof(ClockLineData);
			pData += sizeof(ClockLineData);

			if(linesLoaded == 0)
			{
				pNewFirstLine = pNewLine;
			}
			else
			{
				pNewLastLine->pNext = pNewLine;
			}

			pNewLastLine = pNewLine;
			linesLoaded++;
		}
	}
	while(0);

	if(linesLoaded > 0)
	{
		clearClockLines(&g_pFirstClockLine);
		g_pFirstClockLine = pNewFirstLine;
	}
	else if(g_pFirstClockLine == NULL)
		createDefaultClockLines();

	g_customColors[1] = RGB(g_commonSettings.backgroundRed,
		g_commonSettings.backgroundGreen, g_commonSettings.backgroundBlue);
	ClockLine * pClockLine;
	size_t i;
	for(pClockLine = g_pFirstClockLine, i = 2u;
		(pClockLine != NULL) && (i < CUSTOM_COLORS_NUMBER);
		pClockLine = pClockLine->pNext, i++)
	{
		g_customColors[i] = RGB(pClockLine->data.colorRed,
			pClockLine->data.colorGreen, pClockLine->data.colorBlue);
	}

	free(pConfigData);
}

// END OF void loadSettings()
//==============================================================================

void setLineFontPreviewFont(HWND a_hWnd, const LOGFONTW * a_pFontDescriptor,
	HWND a_hReferenceControl)
{
	if((a_hWnd == 0) || (a_pFontDescriptor == NULL) ||
		(a_hReferenceControl == 0))
	{
		return;
	}

	HFONT hCurrentPreviewFont = (HFONT)SendMessageW(a_hWnd, WM_GETFONT, 0, 0);
	// There is descriptor for the common font shared between controls.
	// It should not be deleted.
	HFONT hCommonFont =
		(HFONT)SendMessageW(a_hReferenceControl, WM_GETFONT, 0, 0);
	HFONT hNewPreviewFont = CreateFontIndirectW(a_pFontDescriptor);
	SendMessageW(a_hWnd, WM_SETFONT, (WPARAM)hNewPreviewFont, 0);
	if((hCurrentPreviewFont != 0) && (hCurrentPreviewFont != hCommonFont))
	{
		DeleteObject(hCurrentPreviewFont);
	}
}

// END OF void setLineFontPreviewFont(HWND a_hWnd,
//		const LOGFONTW * a_pFontDescriptor, HWND a_hReferenceControl)
//==============================================================================

void setLineFontPreviewText(HWND a_hWnd, const LOGFONTW * a_pFontDescriptor)
{
	if((a_hWnd == 0) || (a_pFontDescriptor == NULL))
	{
		return;
	}

	HDC hScreenDC = GetDC(NULL);
	int logPixelsY = GetDeviceCaps(hScreenDC, LOGPIXELSY);
	ReleaseDC(NULL,hScreenDC);
	int fontSize = -MulDiv(a_pFontDescriptor->lfHeight, 72, logPixelsY);

	wchar_t fontDescription[LF_FACESIZE + 5] = {0};
	wsprintf(fontDescription, L"%ls, %d", a_pFontDescriptor->lfFaceName,
		fontSize);
	SetWindowTextW(a_hWnd, fontDescription);
}

// END OF void setLineFontPreviewText(HWND a_hWnd,
//		const LOGFONTW * a_pFontDescriptor)
//==============================================================================

LRESULT APIENTRY SettingsDialogProc(HWND a_hWnd, UINT a_message,
	WPARAM a_wParam, LPARAM a_lParam)
{
	static CommonSettings backupCommonSettings = {0};
	static ClockLine * pSelectedLine = NULL;
	static ClockLine * pBackupFirstLine = NULL;

	if(a_message == WM_INITDIALOG)
	{
		g_modalWindowIsOpened = 1;

		{
			wchar_t caption[SETTINGS_WINDOW_CAPTION_SIZE] = {0};
			swprintf(caption, SETTINGS_WINDOW_CAPTION_SIZE, L"%ls Settings",
				APPLICATION_NAME);
			SetWindowTextW(a_hWnd, caption);
		}

		SetWindowTextW(GetDlgItem(a_hWnd, IDC_LINE_COLOR_STATIC), L"");
		SetWindowTextW(GetDlgItem(a_hWnd, IDC_BACKDROUND_COLOR_STATIC), L"");

		memcpy(&backupCommonSettings, &g_commonSettings,
			sizeof(CommonSettings));
		copyClockLines(&pBackupFirstLine, g_pFirstClockLine);

		HWND hLinesList = GetDlgItem(a_hWnd, IDC_LINES_LIST);
		RECT linesListRect;
		GetClientRect(hLinesList, &linesListRect);
		LVCOLUMNW column;
		ZeroMemory(&column, sizeof(column));
		column.mask = LVCF_WIDTH;
		column.cx = linesListRect.right - linesListRect.left;
		ListView_InsertColumn(hLinesList, 0, &column);

		LVITEMW lineItem;
		ZeroMemory(&lineItem, sizeof(LVITEMW));

		ClockLine * pClockLine = NULL;
		int itemNumber = 0;
		for(pClockLine = g_pFirstClockLine, itemNumber = 0;
			pClockLine != NULL;
			pClockLine = pClockLine->pNext, itemNumber++)
		{
			lineItem.iItem = itemNumber;
			lineItem.mask = LVIF_PARAM | LVIF_TEXT;
			lineItem.lParam = (LPARAM)pClockLine;
			lineItem.pszText = pClockLine->data.format;
			lineItem.cchTextMax = CLOCK_LINE_FORMAT_SIZE;
			ListView_InsertItem(hLinesList, &lineItem);
		}

		ListView_SetItemState(hLinesList, 0, LVIS_SELECTED | LVIS_FOCUSED,
			LVIS_SELECTED | LVIS_FOCUSED);

		SendMessage(GetDlgItem(a_hWnd, IDC_LINE_FORMAT_EDIT), EM_LIMITTEXT,
			CLOCK_LINE_FORMAT_SIZE - 1, 0);
	}
	else if(a_message == WM_DESTROY)
	{
		g_modalWindowIsOpened = 0;
	}
	else if(a_message == WM_CLOSE)
	{
		copyClockLines(&g_pFirstClockLine, pBackupFirstLine);
		clearClockLines(&pBackupFirstLine);
		HFONT hCurrentPreviewFont = (HFONT)SendMessageW(GetDlgItem(a_hWnd,
			IDC_LINE_FONT_STATIC), WM_GETFONT, 0, 0);
		HFONT hCommonFont = (HFONT)SendMessageW(a_hWnd, WM_GETFONT, 0, 0);
		if((hCurrentPreviewFont != 0) && (hCurrentPreviewFont != hCommonFont))
		{
			SendDlgItemMessageW(a_hWnd, IDC_LINE_FONT_STATIC, WM_SETFONT, 0, 0);
			DeleteObject(hCurrentPreviewFont);
		}

		DeleteObject(g_hBackgroundBrush);
		DeleteObject(g_hCurrentLineBrush);

		EndDialog(a_hWnd, IDC_CANCEL);
	}
	else if(a_message == WM_NOTIFY)
	{
		if(((NMHDR *)a_lParam)->idFrom == IDC_LINES_LIST)
		{
			if((((NMHDR *)a_lParam)->code == LVN_ITEMCHANGED) &&
				(((NMLISTVIEW *)a_lParam)->uNewState & LVIS_SELECTED))
			{
				pSelectedLine = (ClockLine *)(((NMLISTVIEW *)a_lParam)->lParam);

				SetWindowTextW(GetDlgItem(a_hWnd, IDC_LINE_FORMAT_EDIT),
					pSelectedLine->data.format);

				setLineFontPreviewFont(GetDlgItem(a_hWnd, IDC_LINE_FONT_STATIC),
					&pSelectedLine->data.fontDescriptor, a_hWnd);
				setLineFontPreviewText(GetDlgItem(a_hWnd, IDC_LINE_FONT_STATIC),
					&pSelectedLine->data.fontDescriptor);

				InvalidateRect(GetDlgItem(a_hWnd, IDC_LINE_COLOR_STATIC),
					NULL, TRUE);
			}
		}
	}
	else if(a_message == WM_COMMAND)
	{
		WORD loWord = LOWORD(a_wParam);
		WORD hiWord = HIWORD(a_wParam);

		if(loWord == IDC_ADD_LINE)
		{
			ClockLine * pNewLine = (ClockLine *)malloc(sizeof(ClockLine));
			setClockLineToDefault(pNewLine);

			if(g_pFirstClockLine == NULL)
			{
				g_pFirstClockLine = pNewLine;
			}
			else
			{
				ClockLine * pLastLine = g_pFirstClockLine;
				while(pLastLine->pNext != NULL)
					pLastLine = pLastLine->pNext;
				pLastLine->pNext = pNewLine;
			}

			HWND hLinesList = GetDlgItem(a_hWnd, IDC_LINES_LIST);
			int itemNumber = ListView_GetItemCount(hLinesList);

			LVITEMW lineItem;
			ZeroMemory(&lineItem, sizeof(LVITEMW));
			lineItem.iItem = itemNumber;
			lineItem.mask = LVIF_PARAM | LVIF_TEXT;
			lineItem.lParam = (LPARAM)pNewLine;
			lineItem.pszText = pNewLine->data.format;
			lineItem.cchTextMax = CLOCK_LINE_FORMAT_SIZE;

			ListView_InsertItem(hLinesList, &lineItem);
			ListView_SetItemState(hLinesList, itemNumber,
				LVIS_SELECTED | LVIS_FOCUSED, LVIS_SELECTED | LVIS_FOCUSED);
			InvalidateRect(g_hMainWindow, NULL, FALSE);
		}
		if(loWord == IDC_REMOVE_LINE)
		{
			if(g_pFirstClockLine == NULL)
				return FALSE;

			HWND hLinesList = GetDlgItem(a_hWnd, IDC_LINES_LIST);
			if(pSelectedLine == g_pFirstClockLine)
			{
				ClockLine * pSecondLine = g_pFirstClockLine->pNext;
				free(g_pFirstClockLine);
				g_pFirstClockLine = pSecondLine;
				ListView_DeleteItem(hLinesList, 0);
				if(g_pFirstClockLine == NULL)
				{
					pSelectedLine = NULL;
				}
				else
				{
					ListView_SetItemState(hLinesList, 0,
					LVIS_SELECTED | LVIS_FOCUSED,
					LVIS_SELECTED | LVIS_FOCUSED);
				}
			}
			else
			{
				int lineNumber = ListView_GetNextItem(hLinesList, -1,
					LVNI_SELECTED);
				ClockLine * pPreviousLine = g_pFirstClockLine;
				while(pPreviousLine->pNext != pSelectedLine)
					pPreviousLine = pPreviousLine->pNext;
				pPreviousLine->pNext = pSelectedLine->pNext;
				free(pSelectedLine);
				ListView_DeleteItem(hLinesList, lineNumber);
				if(lineNumber == ListView_GetItemCount(hLinesList))
					lineNumber--;
				ListView_SetItemState(hLinesList, lineNumber,
					LVIS_SELECTED | LVIS_FOCUSED,
					LVIS_SELECTED | LVIS_FOCUSED);
			}

			InvalidateRect(g_hMainWindow, NULL, FALSE);
		}
		if(loWord == IDC_MOVE_LINE_UP)
		{
			if(pSelectedLine == g_pFirstClockLine)
				return FALSE;

			HWND hLinesList = GetDlgItem(a_hWnd, IDC_LINES_LIST);
			int lineNumber = ListView_GetNextItem(hLinesList, -1,
				LVNI_SELECTED);

			ClockLine * pPreviousLine = g_pFirstClockLine;
			while(pPreviousLine->pNext != pSelectedLine)
				pPreviousLine = pPreviousLine->pNext;

			ListView_SetItemText(hLinesList, lineNumber - 1, 0,
				pSelectedLine->data.format);
			ListView_SetItemText(hLinesList, lineNumber, 0,
				pPreviousLine->data.format);

			ClockLineData tempClockLineData;
			memcpy(&tempClockLineData, &pSelectedLine->data,
				sizeof(ClockLineData));
			memcpy(&pSelectedLine->data, &pPreviousLine->data,
				sizeof(ClockLineData));
			memcpy(&pPreviousLine->data, &tempClockLineData,
				sizeof(ClockLineData));

			ListView_SetItemState(hLinesList, lineNumber - 1,
				LVIS_SELECTED | LVIS_FOCUSED, LVIS_SELECTED | LVIS_FOCUSED);
			InvalidateRect(g_hMainWindow, NULL, FALSE);
		}
		if(loWord == IDC_MOVE_LINE_DOWN)
		{
			if(pSelectedLine == NULL)
				return FALSE;
			else if(pSelectedLine->pNext == NULL)
				return FALSE;

			HWND hLinesList = GetDlgItem(a_hWnd, IDC_LINES_LIST);
			int lineNumber = ListView_GetNextItem(hLinesList, -1,
				LVNI_SELECTED);

			ClockLine * pNextLine = pSelectedLine->pNext;

			ListView_SetItemText(hLinesList, lineNumber + 1, 0,
				pSelectedLine->data.format);
			ListView_SetItemText(hLinesList, lineNumber, 0,
				pNextLine->data.format);

			ClockLineData tempClockLineData;
			memcpy(&tempClockLineData, &pSelectedLine->data,
				sizeof(ClockLineData));
			memcpy(&pSelectedLine->data, &pNextLine->data,
				sizeof(ClockLineData));
			memcpy(&pNextLine->data, &tempClockLineData,
				sizeof(ClockLineData));

			ListView_SetItemState(hLinesList, lineNumber + 1,
				LVIS_SELECTED | LVIS_FOCUSED, LVIS_SELECTED | LVIS_FOCUSED);
			InvalidateRect(g_hMainWindow, NULL, FALSE);
		}
		if(loWord == IDC_OK)
		{
			clearClockLines(&pBackupFirstLine);
			EndDialog(a_hWnd, IDC_OK);
		}
		else if(loWord == IDC_CANCEL)
		{
			memcpy(&g_commonSettings, &backupCommonSettings,
				sizeof(CommonSettings));
			copyClockLines(&g_pFirstClockLine, pBackupFirstLine);
			InvalidateRect(g_hMainWindow, NULL, FALSE);
			clearClockLines(&pBackupFirstLine);
			EndDialog(a_hWnd, IDC_CANCEL);
		}
		else if(loWord == IDC_LINE_FORMAT_HELP)
		{
			wchar_t * formatHelpStringFormat = L"Line format string "
				L"must be up to %d characters long and may contain any "
				L"of the following variables:\n\n"
				L"%%HH\t\thours with 24 hours format and zero padding.\n"
				L"%%hh\t\thours with 12 hours format and zero padding.\n"
				L"%%mm\t\tminutes with zero padding.\n"
				L"%%ss\t\tseconds with zero padding.\n"
				L"%%H\t\thours with 24 hours format without zero padding.\n"
				L"%%h\t\thours with 12 hours format without zero padding.\n"
				L"%%m\t\tminutes without zero padding.\n"
				L"%%s\t\tseconds without zero padding.\n"
				L"%%tt\t\tAM/PM mark if present in region settings.\n"
				L"%%dddd\t\tday of week complete name.\n"
				L"%%ddd\t\tday of week shortened name.\n"
				L"%%dd\t\tday of month number with zero padding.\n"
				L"%%d\t\tday of month number without zero padding.\n"
				L"%%MMMM\t\tmonth complete name.\n"
				L"%%MMM\t\tmonth shortened name.\n"
				L"%%MM\t\tmonth number with zero padding.\n"
				L"%%M\t\tmonth number without zero padding.\n"
				L"%%yyyyy, %%yyyy\tyear complete.\n"
				L"%%yy\t\tyear last two digits with zero padding.\n"
				L"%%y\t\tyear last two digits without zero padding.\n"
				L"%%gg, %%g\t\tera."
				;

			wchar_t formatHelpString[LINE_FORMAT_HELP_STRING_SIZE] = {0};
			swprintf(formatHelpString, LINE_FORMAT_HELP_STRING_SIZE,
				formatHelpStringFormat, CLOCK_LINE_FORMAT_SIZE - 1);

			MessageBoxW(g_hMainWindow, formatHelpString, L"Clock line format",
				MB_ICONINFORMATION);
		}
		else if(loWord == IDC_LINE_FORMAT_EDIT)
		{
			HWND hLineFormatEdit = GetDlgItem(a_hWnd, IDC_LINE_FORMAT_EDIT);
			if((hiWord == EN_CHANGE) && (GetFocus() == hLineFormatEdit))
			{
				GetWindowTextW(hLineFormatEdit, pSelectedLine->data.format,
					CLOCK_LINE_FORMAT_SIZE);
				HWND hLinesList = GetDlgItem(a_hWnd, IDC_LINES_LIST);
				int lineNumber = ListView_GetNextItem(hLinesList, -1,
					LVNI_SELECTED);
				ListView_SetItemText(hLinesList, lineNumber, 0,
					pSelectedLine->data.format);
				InvalidateRect(g_hMainWindow, NULL, FALSE);
			}
		}
		else if(loWord == IDC_CHOOSE_LINE_FONT)
		{
			CHOOSEFONTW fontSettings;
			ZeroMemory(&fontSettings, sizeof(fontSettings));
			fontSettings.lStructSize = sizeof(fontSettings);
			fontSettings.hwndOwner = g_hMainWindow;
			fontSettings.Flags = CF_INITTOLOGFONTSTRUCT | CF_EFFECTS;
			fontSettings.lpLogFont = &pSelectedLine->data.fontDescriptor;

			ChooseFontW(&fontSettings);

			setLineFontPreviewFont(GetDlgItem(a_hWnd, IDC_LINE_FONT_STATIC),
				&pSelectedLine->data.fontDescriptor, a_hWnd);
			setLineFontPreviewText(GetDlgItem(a_hWnd, IDC_LINE_FONT_STATIC),
				&pSelectedLine->data.fontDescriptor);

			InvalidateRect(g_hMainWindow, NULL, FALSE);
		}
		else if(loWord == IDC_CHOOSE_LINE_COLOR)
		{
			CHOOSECOLORW lineColorSettings;
			ZeroMemory(&lineColorSettings, sizeof(lineColorSettings));
			lineColorSettings.lStructSize = sizeof(lineColorSettings);
			lineColorSettings.hwndOwner = g_hMainWindow;
			lineColorSettings.Flags = CC_ANYCOLOR | CC_FULLOPEN | CC_RGBINIT |
				CC_SOLIDCOLOR;
			lineColorSettings.rgbResult = RGB(pSelectedLine->data.colorRed,
				pSelectedLine->data.colorGreen, pSelectedLine->data.colorBlue);
			lineColorSettings.lpCustColors = g_customColors;

			ChooseColorW(&lineColorSettings);

			pSelectedLine->data.colorRed =
				GetRValue(lineColorSettings.rgbResult);
			pSelectedLine->data.colorGreen =
				GetGValue(lineColorSettings.rgbResult);
			pSelectedLine->data.colorBlue =
				GetBValue(lineColorSettings.rgbResult);

			InvalidateRect(GetDlgItem(a_hWnd, IDC_LINE_COLOR_STATIC),
				NULL, TRUE);
			InvalidateRect(g_hMainWindow, NULL, FALSE);
		}
		else if(loWord == IDC_CHOOSE_BACKGROUND_COLOR)
		{
			CHOOSECOLORW lineColorSettings;
			ZeroMemory(&lineColorSettings, sizeof(lineColorSettings));
			lineColorSettings.lStructSize = sizeof(lineColorSettings);
			lineColorSettings.hwndOwner = g_hMainWindow;
			lineColorSettings.Flags = CC_ANYCOLOR | CC_FULLOPEN | CC_RGBINIT |
				CC_SOLIDCOLOR;
			lineColorSettings.rgbResult = RGB(g_commonSettings.backgroundRed,
				g_commonSettings.backgroundGreen,
				g_commonSettings.backgroundBlue);
			lineColorSettings.lpCustColors = g_customColors;

			ChooseColorW(&lineColorSettings);

			g_commonSettings.backgroundRed =
				GetRValue(lineColorSettings.rgbResult);
			g_commonSettings.backgroundGreen =
				GetGValue(lineColorSettings.rgbResult);
			g_commonSettings.backgroundBlue =
				GetBValue(lineColorSettings.rgbResult);

			InvalidateRect(GetDlgItem(a_hWnd, IDC_BACKDROUND_COLOR_STATIC),
				NULL, TRUE);
			InvalidateRect(g_hMainWindow, NULL, FALSE);
		}
	}
	else if(a_message == WM_CTLCOLORSTATIC)
	{
		if((HWND)a_lParam == GetDlgItem(a_hWnd, IDC_LINE_COLOR_STATIC))
		{
			if(pSelectedLine == NULL)
				return FALSE;

			DeleteObject(g_hCurrentLineBrush);
			g_hCurrentLineBrush = CreateSolidBrush(RGB(
				pSelectedLine->data.colorRed,
				pSelectedLine->data.colorGreen,
				pSelectedLine->data.colorBlue));
			return (INT_PTR)g_hCurrentLineBrush;
		}
		else if((HWND)a_lParam ==
			GetDlgItem(a_hWnd, IDC_BACKDROUND_COLOR_STATIC))
		{
			DeleteObject(g_hBackgroundBrush);
			g_hBackgroundBrush = CreateSolidBrush(RGB(
				g_commonSettings.backgroundRed,
				g_commonSettings.backgroundGreen,
				g_commonSettings.backgroundBlue));
			return (INT_PTR)g_hBackgroundBrush;
		}
	}

	return FALSE;
}

// END OF LRESULT APIENTRY SettingsDialogProc(HWND a_hWnd, UINT a_message,
//		WPARAM a_wParam, LPARAM a_lParam)
//==============================================================================
