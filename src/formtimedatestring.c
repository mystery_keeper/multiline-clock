#include <wchar.h>

#include "globaldefines.h"

void formTimeDateString(const wchar_t * a_format, wchar_t * a_outputString,
	unsigned int a_maxOutputStringLength)
{
	memset(a_outputString, 0, (a_maxOutputStringLength + 1) * sizeof(wchar_t));

	SYSTEMTIME localTime;
	GetLocalTime(&localTime);

	wchar_t * stringBuffer = NULL;
	int stringLengthNeeded = 0;

	unsigned int formatIterator = 0, outputIterator = 0;

	while((a_format[formatIterator] != 0) &&
		(outputIterator < a_maxOutputStringLength))
	{
		// Hours: 24 hours a_format with zero padding
		if(wcsncmp(a_format + formatIterator, L"%HH", wcslen(L"%HH")) == 0)
		{
			stringLengthNeeded = GetTimeFormatW(LOCALE_USER_DEFAULT, 0,
				&localTime, L"HH", stringBuffer, 0) - 1;

			if(stringLengthNeeded < 1)
			{
				formatIterator += wcslen(L"%HH");
				continue;
			}

			if((a_maxOutputStringLength - outputIterator) < stringLengthNeeded)
			{
				a_outputString[outputIterator] = 0;
				return;
			}

			stringBuffer =
				(wchar_t *)malloc((stringLengthNeeded + 1) * sizeof(wchar_t));
			GetTimeFormatW(LOCALE_USER_DEFAULT, 0, &localTime, L"HH",
				stringBuffer, stringLengthNeeded + 1);
			wcscpy(a_outputString + outputIterator, stringBuffer);
			free(stringBuffer);

			formatIterator += wcslen(L"%HH");
			outputIterator += stringLengthNeeded;
		}

		// Hours: 12 hours a_format with zero padding
		else if(wcsncmp(a_format + formatIterator, L"%hh", wcslen(L"%hh")) == 0)
		{
			stringLengthNeeded = GetTimeFormatW(LOCALE_USER_DEFAULT, 0,
				&localTime, L"hh", stringBuffer, 0) - 1;

			if(stringLengthNeeded < 1)
			{
				formatIterator += wcslen(L"%hh");
				continue;
			}

			if((a_maxOutputStringLength - outputIterator) < stringLengthNeeded)
			{
				a_outputString[outputIterator] = 0;
				return;
			}

			stringBuffer =
				(wchar_t *)malloc((stringLengthNeeded + 1) * sizeof(wchar_t));
			GetTimeFormatW(LOCALE_USER_DEFAULT, 0, &localTime, L"hh",
				stringBuffer, stringLengthNeeded + 1);
			wcscpy(a_outputString + outputIterator, stringBuffer);
			free(stringBuffer);

			formatIterator += wcslen(L"%hh");
			outputIterator += stringLengthNeeded;
		}

		// Minutes: with zero padding
		else if(wcsncmp(a_format + formatIterator, L"%mm", wcslen(L"%mm")) == 0)
		{
			stringLengthNeeded = GetTimeFormatW(LOCALE_USER_DEFAULT, 0,
				&localTime, L"mm", stringBuffer, 0) - 1;

			if(stringLengthNeeded < 1)
			{
				formatIterator += wcslen(L"%mm");
				continue;
			}

			if((a_maxOutputStringLength - outputIterator) < stringLengthNeeded)
			{
				a_outputString[outputIterator] = 0;
				return;
			}

			stringBuffer =
				(wchar_t *)malloc((stringLengthNeeded + 1) * sizeof(wchar_t));
			GetTimeFormatW(LOCALE_USER_DEFAULT, 0, &localTime, L"mm",
				stringBuffer, stringLengthNeeded + 1);
			wcscpy(a_outputString + outputIterator, stringBuffer);
			free(stringBuffer);

			formatIterator += wcslen(L"%mm");
			outputIterator += stringLengthNeeded;
		}

		// Seconds: with zero padding
		else if(wcsncmp(a_format + formatIterator, L"%ss", wcslen(L"%ss")) == 0)
		{
			stringLengthNeeded = GetTimeFormatW(LOCALE_USER_DEFAULT, 0,
				&localTime, L"ss", stringBuffer, 0) - 1;

			if(stringLengthNeeded < 1)
			{
				formatIterator += wcslen(L"%ss");
				continue;
			}

			if((a_maxOutputStringLength - outputIterator) < stringLengthNeeded)
			{
				a_outputString[outputIterator] = 0;
				return;
			}

			stringBuffer =
				(wchar_t *)malloc((stringLengthNeeded + 1) * sizeof(wchar_t));
			GetTimeFormatW(LOCALE_USER_DEFAULT, 0, &localTime, L"ss",
				stringBuffer, stringLengthNeeded + 1);
			wcscpy(a_outputString + outputIterator, stringBuffer);
			free(stringBuffer);

			formatIterator += wcslen(L"%ss");
			outputIterator += stringLengthNeeded;
		}

		// Hours: 24 hours a_format without zero padding
		if(wcsncmp(a_format + formatIterator, L"%H", wcslen(L"%H")) == 0)
		{
			stringLengthNeeded = GetTimeFormatW(LOCALE_USER_DEFAULT, 0,
				&localTime, L"H", stringBuffer, 0) - 1;

			if(stringLengthNeeded < 1)
			{
				formatIterator += wcslen(L"%H");
				continue;
			}

			if((a_maxOutputStringLength - outputIterator) < stringLengthNeeded)
			{
				a_outputString[outputIterator] = 0;
				return;
			}

			stringBuffer =
				(wchar_t *)malloc((stringLengthNeeded + 1) * sizeof(wchar_t));
			GetTimeFormatW(LOCALE_USER_DEFAULT, 0, &localTime, L"H",
				stringBuffer, stringLengthNeeded + 1);
			wcscpy(a_outputString + outputIterator, stringBuffer);
			free(stringBuffer);

			formatIterator += wcslen(L"%H");
			outputIterator += stringLengthNeeded;
		}

		// Hours: 12 hours a_format without zero padding
		else if(wcsncmp(a_format + formatIterator, L"%h", wcslen(L"%h")) == 0)
		{
			stringLengthNeeded = GetTimeFormatW(LOCALE_USER_DEFAULT, 0,
				&localTime, L"h", stringBuffer, 0) - 1;

			if(stringLengthNeeded < 1)
			{
				formatIterator += wcslen(L"%h");
				continue;
			}

			if((a_maxOutputStringLength - outputIterator) < stringLengthNeeded)
			{
				a_outputString[outputIterator] = 0;
				return;
			}

			stringBuffer =
				(wchar_t *)malloc((stringLengthNeeded + 1) * sizeof(wchar_t));
			GetTimeFormatW(LOCALE_USER_DEFAULT, 0, &localTime, L"h",
				stringBuffer, stringLengthNeeded + 1);
			wcscpy(a_outputString + outputIterator, stringBuffer);
			free(stringBuffer);

			formatIterator += wcslen(L"%h");
			outputIterator += stringLengthNeeded;
		}

		// Minutes: without zero padding
		else if(wcsncmp(a_format + formatIterator, L"%m", wcslen(L"%m")) == 0)
		{
			stringLengthNeeded = GetTimeFormatW(LOCALE_USER_DEFAULT, 0,
				&localTime, L"m", stringBuffer, 0) - 1;

			if(stringLengthNeeded < 1)
			{
				formatIterator += wcslen(L"%m");
				continue;
			}

			if((a_maxOutputStringLength - outputIterator) < stringLengthNeeded)
			{
				a_outputString[outputIterator] = 0;
				return;
			}

			stringBuffer =
				(wchar_t *)malloc((stringLengthNeeded + 1) * sizeof(wchar_t));
			GetTimeFormatW(LOCALE_USER_DEFAULT, 0, &localTime, L"m",
				stringBuffer, stringLengthNeeded + 1);
			wcscpy(a_outputString + outputIterator, stringBuffer);
			free(stringBuffer);

			formatIterator += wcslen(L"%m");
			outputIterator += stringLengthNeeded;
		}

		// Seconds: without zero padding
		else if(wcsncmp(a_format + formatIterator, L"%s", wcslen(L"%s")) == 0)
		{
			stringLengthNeeded = GetTimeFormatW(LOCALE_USER_DEFAULT, 0,
				&localTime, L"s", stringBuffer, 0) - 1;

			if(stringLengthNeeded < 1)
			{
				formatIterator += wcslen(L"%s");
				continue;
			}

			if((a_maxOutputStringLength - outputIterator) < stringLengthNeeded)
			{
				a_outputString[outputIterator] = 0;
				return;
			}

			stringBuffer =
				(wchar_t *)malloc((stringLengthNeeded + 1) * sizeof(wchar_t));
			GetTimeFormatW(LOCALE_USER_DEFAULT, 0, &localTime, L"s",
				stringBuffer, stringLengthNeeded + 1);
			wcscpy(a_outputString + outputIterator, stringBuffer);
			free(stringBuffer);

			formatIterator += wcslen(L"%s");
			outputIterator += stringLengthNeeded;
		}

		// AM/PM mark
		else if(wcsncmp(a_format + formatIterator, L"%tt", wcslen(L"%tt")) == 0)
		{
			stringLengthNeeded = GetTimeFormatW(LOCALE_USER_DEFAULT, 0,
				&localTime, L"tt", stringBuffer, 0) - 1;

			if(stringLengthNeeded < 1)
			{
				formatIterator += wcslen(L"%tt");
				continue;
			}

			if((a_maxOutputStringLength - outputIterator) < stringLengthNeeded)
			{
				a_outputString[outputIterator] = 0;
				return;
			}

			stringBuffer =
				(wchar_t *)malloc((stringLengthNeeded + 1) * sizeof(wchar_t));
			GetTimeFormatW(LOCALE_USER_DEFAULT, 0, &localTime, L"tt",
				stringBuffer, stringLengthNeeded + 1);
			wcscpy(a_outputString + outputIterator, stringBuffer);
			free(stringBuffer);

			formatIterator += wcslen(L"%tt");
			outputIterator += stringLengthNeeded;
		}

		// Day of week: complete
		else if(wcsncmp(a_format + formatIterator, L"%dddd",
			wcslen(L"%dddd")) == 0)
		{
			stringLengthNeeded = GetDateFormatW(LOCALE_USER_DEFAULT, 0,
				&localTime, L"dddd", stringBuffer, 0) - 1;

			if(stringLengthNeeded < 1)
			{
				formatIterator += wcslen(L"%dddd");
				continue;
			}

			if((a_maxOutputStringLength - outputIterator) < stringLengthNeeded)
			{
				a_outputString[outputIterator] = 0;
				return;
			}

			stringBuffer =
				(wchar_t *)malloc((stringLengthNeeded + 1) * sizeof(wchar_t));
			GetDateFormatW(LOCALE_USER_DEFAULT, 0, &localTime, L"dddd",
				stringBuffer, stringLengthNeeded + 1);
			wcscpy(a_outputString + outputIterator, stringBuffer);
			free(stringBuffer);

			formatIterator += wcslen(L"%dddd");
			outputIterator += stringLengthNeeded;
		}

		// Day of week: short
		else if(wcsncmp(a_format + formatIterator, L"%ddd",
			wcslen(L"%ddd")) == 0)
		{
			stringLengthNeeded = GetDateFormatW(LOCALE_USER_DEFAULT, 0,
				&localTime, L"ddd", stringBuffer, 0) - 1;

			if(stringLengthNeeded < 1)
			{
				formatIterator += wcslen(L"%ddd");
				continue;
			}

			if((a_maxOutputStringLength - outputIterator) < stringLengthNeeded)
			{
				a_outputString[outputIterator] = 0;
				return;
			}

			stringBuffer =
				(wchar_t *)malloc((stringLengthNeeded + 1) * sizeof(wchar_t));
			GetDateFormatW(LOCALE_USER_DEFAULT, 0, &localTime, L"ddd",
				stringBuffer, stringLengthNeeded + 1);
			wcscpy(a_outputString + outputIterator, stringBuffer);
			free(stringBuffer);

			formatIterator += wcslen(L"%ddd");
			outputIterator += stringLengthNeeded;
		}

		// Day of month: with zero padding
		else if(wcsncmp(a_format + formatIterator, L"%dd",
			wcslen(L"%dd")) == 0)
		{
			stringLengthNeeded = GetDateFormatW(LOCALE_USER_DEFAULT, 0,
				&localTime, L"dd", stringBuffer, 0) - 1;

			if(stringLengthNeeded < 1)
			{
				formatIterator += wcslen(L"%dd");
				continue;
			}

			if((a_maxOutputStringLength - outputIterator) < stringLengthNeeded)
			{
				a_outputString[outputIterator] = 0;
				return;
			}

			stringBuffer =
				(wchar_t *)malloc((stringLengthNeeded + 1) * sizeof(wchar_t));
			GetDateFormatW(LOCALE_USER_DEFAULT, 0, &localTime, L"dd",
				stringBuffer, stringLengthNeeded + 1);
			wcscpy(a_outputString + outputIterator, stringBuffer);
			free(stringBuffer);

			formatIterator += wcslen(L"%dd");
			outputIterator += stringLengthNeeded;
		}

		// Day of month: without zero padding
		else if(wcsncmp(a_format + formatIterator, L"%d",
			wcslen(L"%d")) == 0)
		{
			stringLengthNeeded = GetDateFormatW(LOCALE_USER_DEFAULT, 0,
				&localTime, L"d", stringBuffer, 0) - 1;

			if(stringLengthNeeded < 1)
			{
				formatIterator += wcslen(L"%d");
				continue;
			}

			if((a_maxOutputStringLength - outputIterator) < stringLengthNeeded)
			{
				a_outputString[outputIterator] = 0;
				return;
			}

			stringBuffer =
				(wchar_t *)malloc((stringLengthNeeded + 1) * sizeof(wchar_t));
			GetDateFormatW(LOCALE_USER_DEFAULT, 0, &localTime, L"d",
				stringBuffer, stringLengthNeeded + 1);
			wcscpy(a_outputString + outputIterator, stringBuffer);
			free(stringBuffer);

			formatIterator += wcslen(L"%d");
			outputIterator += stringLengthNeeded;
		}

		// Month: complete name
		else if(wcsncmp(a_format + formatIterator, L"%MMMM",
			wcslen(L"%MMMM")) == 0)
		{
			stringLengthNeeded = GetDateFormatW(LOCALE_USER_DEFAULT, 0,
				&localTime, L"MMMM", stringBuffer, 0) - 1;

			if(stringLengthNeeded < 1)
			{
				formatIterator += wcslen(L"%MMMM");
				continue;
			}

			if((a_maxOutputStringLength - outputIterator) < stringLengthNeeded)
			{
				a_outputString[outputIterator] = 0;
				return;
			}

			stringBuffer =
				(wchar_t *)malloc((stringLengthNeeded + 1) * sizeof(wchar_t));
			GetDateFormatW(LOCALE_USER_DEFAULT, 0, &localTime, L"MMMM",
				stringBuffer, stringLengthNeeded + 1);
			wcscpy(a_outputString + outputIterator, stringBuffer);
			free(stringBuffer);

			formatIterator += wcslen(L"%MMMM");
			outputIterator += stringLengthNeeded;
		}

		// Month: short name
		else if(wcsncmp(a_format + formatIterator, L"%MMM",
			wcslen(L"%MMM")) == 0)
		{
			stringLengthNeeded = GetDateFormatW(LOCALE_USER_DEFAULT, 0,
				&localTime, L"MMM", stringBuffer, 0) - 1;

			if(stringLengthNeeded < 1)
			{
				formatIterator += wcslen(L"%MMM");
				continue;
			}

			if((a_maxOutputStringLength - outputIterator) < stringLengthNeeded)
			{
				a_outputString[outputIterator] = 0;
				return;
			}

			stringBuffer =
				(wchar_t *)malloc((stringLengthNeeded + 1) * sizeof(wchar_t));
			GetDateFormatW(LOCALE_USER_DEFAULT, 0, &localTime, L"MMM",
				stringBuffer, stringLengthNeeded + 1);
			wcscpy(a_outputString + outputIterator, stringBuffer);
			free(stringBuffer);

			formatIterator += wcslen(L"%MMM");
			outputIterator += stringLengthNeeded;
		}

		// Month: number with zero padding
		else if(wcsncmp(a_format + formatIterator, L"%MM",
			wcslen(L"%MM")) == 0)
		{
			stringLengthNeeded = GetDateFormatW(LOCALE_USER_DEFAULT, 0,
				&localTime, L"MM", stringBuffer, 0) - 1;

			if(stringLengthNeeded < 1)
			{
				formatIterator += wcslen(L"%MM");
				continue;
			}

			if((a_maxOutputStringLength - outputIterator) < stringLengthNeeded)
			{
				a_outputString[outputIterator] = 0;
				return;
			}

			stringBuffer =
				(wchar_t *)malloc((stringLengthNeeded + 1) * sizeof(wchar_t));
			GetDateFormatW(LOCALE_USER_DEFAULT, 0, &localTime, L"MM",
				stringBuffer, stringLengthNeeded + 1);
			wcscpy(a_outputString + outputIterator, stringBuffer);
			free(stringBuffer);

			formatIterator += wcslen(L"%MM");
			outputIterator += stringLengthNeeded;
		}

		// Month: number without zero padding
		else if(wcsncmp(a_format + formatIterator, L"%M",
			wcslen(L"%M")) == 0)
		{
			stringLengthNeeded = GetDateFormatW(LOCALE_USER_DEFAULT, 0,
				&localTime, L"M", stringBuffer, 0) - 1;

			if(stringLengthNeeded < 1)
			{
				formatIterator += wcslen(L"%M");
				continue;
			}

			if((a_maxOutputStringLength - outputIterator) < stringLengthNeeded)
			{
				a_outputString[outputIterator] = 0;
				return;
			}

			stringBuffer =
				(wchar_t *)malloc((stringLengthNeeded + 1) * sizeof(wchar_t));
			GetDateFormatW(LOCALE_USER_DEFAULT, 0, &localTime, L"M",
				stringBuffer, stringLengthNeeded + 1);
			wcscpy(a_outputString + outputIterator, stringBuffer);
			free(stringBuffer);

			formatIterator += wcslen(L"%M");
			outputIterator += stringLengthNeeded;
		}

		// Year: complete
		else if(wcsncmp(a_format + formatIterator, L"%yyyyy",
			wcslen(L"%yyyyy")) == 0)
		{
			stringLengthNeeded = GetDateFormatW(LOCALE_USER_DEFAULT, 0,
				&localTime, L"yyyyy", stringBuffer, 0) - 1;

			if(stringLengthNeeded < 1)
			{
				formatIterator += wcslen(L"%yyyyy");
				continue;
			}

			if((a_maxOutputStringLength - outputIterator) < stringLengthNeeded)
			{
				a_outputString[outputIterator] = 0;
				return;
			}

			stringBuffer =
				(wchar_t *)malloc((stringLengthNeeded + 1) * sizeof(wchar_t));
			GetDateFormatW(LOCALE_USER_DEFAULT, 0, &localTime, L"yyyyy",
				stringBuffer, stringLengthNeeded + 1);
			wcscpy(a_outputString + outputIterator, stringBuffer);
			free(stringBuffer);

			formatIterator += wcslen(L"%yyyyy");
			outputIterator += stringLengthNeeded;
		}

		// Year: complete
		else if(wcsncmp(a_format + formatIterator, L"%yyyy",
			wcslen(L"%yyyy")) == 0)
		{
			stringLengthNeeded = GetDateFormatW(LOCALE_USER_DEFAULT, 0,
				&localTime, L"yyyy", stringBuffer, 0) - 1;

			if(stringLengthNeeded < 1)
			{
				formatIterator += wcslen(L"%yyyy");
				continue;
			}

			if((a_maxOutputStringLength - outputIterator) < stringLengthNeeded)
			{
				a_outputString[outputIterator] = 0;
				return;
			}

			stringBuffer =
				(wchar_t *)malloc((stringLengthNeeded + 1) * sizeof(wchar_t));
			GetDateFormatW(LOCALE_USER_DEFAULT, 0, &localTime, L"yyyy",
				stringBuffer, stringLengthNeeded + 1);
			wcscpy(a_outputString + outputIterator, stringBuffer);
			free(stringBuffer);

			formatIterator += wcslen(L"%yyyy");
			outputIterator += stringLengthNeeded;
		}

		// Year: last two digits with zero padding
		else if(wcsncmp(a_format + formatIterator, L"%yy",
			wcslen(L"%yy")) == 0)
		{
			stringLengthNeeded = GetDateFormatW(LOCALE_USER_DEFAULT, 0,
				&localTime, L"yy", stringBuffer, 0) - 1;

			if(stringLengthNeeded < 1)
			{
				formatIterator += wcslen(L"%yy");
				continue;
			}

			if((a_maxOutputStringLength - outputIterator) < stringLengthNeeded)
			{
				a_outputString[outputIterator] = 0;
				return;
			}

			stringBuffer =
				(wchar_t *)malloc((stringLengthNeeded + 1) * sizeof(wchar_t));
			GetDateFormatW(LOCALE_USER_DEFAULT, 0, &localTime, L"yy",
				stringBuffer, stringLengthNeeded + 1);
			wcscpy(a_outputString + outputIterator, stringBuffer);
			free(stringBuffer);

			formatIterator += wcslen(L"%yy");
			outputIterator += stringLengthNeeded;
		}

		// Year: last two digits without zero padding
		else if(wcsncmp(a_format + formatIterator, L"%y",
			wcslen(L"%y")) == 0)
		{
			stringLengthNeeded = GetDateFormatW(LOCALE_USER_DEFAULT, 0,
				&localTime, L"y", stringBuffer, 0) - 1;

			if(stringLengthNeeded < 1)
			{
				formatIterator += wcslen(L"%y");
				continue;
			}

			if((a_maxOutputStringLength - outputIterator) < stringLengthNeeded)
			{
				a_outputString[outputIterator] = 0;
				return;
			}

			stringBuffer =
				(wchar_t *)malloc((stringLengthNeeded + 1) * sizeof(wchar_t));
			GetDateFormatW(LOCALE_USER_DEFAULT, 0, &localTime, L"y",
				stringBuffer, stringLengthNeeded + 1);
			wcscpy(a_outputString + outputIterator, stringBuffer);
			free(stringBuffer);

			formatIterator += wcslen(L"%y");
			outputIterator += stringLengthNeeded;
		}

		// Era
		else if(wcsncmp(a_format + formatIterator, L"%gg",
			wcslen(L"%gg")) == 0)
		{
			stringLengthNeeded = GetDateFormatW(LOCALE_USER_DEFAULT, 0,
				&localTime, L"gg", stringBuffer, 0) - 1;

			if(stringLengthNeeded < 1)
			{
				formatIterator += wcslen(L"%gg");
				continue;
			}

			if((a_maxOutputStringLength - outputIterator) < stringLengthNeeded)
			{
				a_outputString[outputIterator] = 0;
				return;
			}

			stringBuffer =
				(wchar_t *)malloc((stringLengthNeeded + 1) * sizeof(wchar_t));
			GetDateFormatW(LOCALE_USER_DEFAULT, 0, &localTime, L"gg",
				stringBuffer, stringLengthNeeded + 1);
			wcscpy(a_outputString + outputIterator, stringBuffer);
			free(stringBuffer);

			formatIterator += wcslen(L"%gg");
			outputIterator += stringLengthNeeded;
		}

		// Era
		else if(wcsncmp(a_format + formatIterator, L"%g",
			wcslen(L"%g")) == 0)
		{
			stringLengthNeeded = GetDateFormatW(LOCALE_USER_DEFAULT, 0,
				&localTime, L"g", stringBuffer, 0) - 1;

			if(stringLengthNeeded < 1)
			{
				formatIterator += wcslen(L"%g");
				continue;
			}

			if((a_maxOutputStringLength - outputIterator) < stringLengthNeeded)
			{
				a_outputString[outputIterator] = 0;
				return;
			}

			stringBuffer =
				(wchar_t *)malloc((stringLengthNeeded + 1) * sizeof(wchar_t));
			GetDateFormatW(LOCALE_USER_DEFAULT, 0, &localTime, L"g",
				stringBuffer, stringLengthNeeded + 1);
			wcscpy(a_outputString + outputIterator, stringBuffer);
			free(stringBuffer);

			formatIterator += wcslen(L"%g");
			outputIterator += stringLengthNeeded;
		}

		// Common characters
		else
		{
			a_outputString[outputIterator] = a_format[formatIterator];
			formatIterator++;
			outputIterator++;
		}
	}
}
