#ifndef GLOBALDEFINES_H_INCLUDED
#define GLOBALDEFINES_H_INCLUDED

#define CLOCK_LINE_FORMAT_SIZE 51
#define CUSTOM_COLORS_NUMBER 16

#include <windows.h>
#include <stdint.h>

#include "../resources/resources.h"

//==============================================================================
// Enums:



// END OF Enums
//==============================================================================
// Structures:

typedef struct tagClockLineData
{
	wchar_t format[CLOCK_LINE_FORMAT_SIZE];
	LOGFONTW fontDescriptor;
	uint8_t colorRed;
	uint8_t colorGreen;
	uint8_t colorBlue;
} ClockLineData;

typedef struct tagClockLine
{
	ClockLineData data;
	int voffset;
	int height;
	struct tagClockLine * pNext;
} ClockLine;

typedef struct tagCommonSettings
{
	uint8_t backgroundRed;
	uint8_t backgroundGreen;
	uint8_t backgroundBlue;
	uint32_t textMarginPixels;
	BOOL stayOnTop;
} CommonSettings;

// END OF Structures
//==============================================================================
// Constants:

extern const wchar_t APPLICATION_NAME[];

extern const UINT_PTR IDM_CENTER_WINDOW_ON_SCREEN;
extern const UINT_PTR IDM_STAY_ON_TOP;
extern const UINT_PTR IDM_SETTINGS;
extern const UINT_PTR IDM_ABOUT;
extern const UINT_PTR IDM_QUIT;

extern const UINT IDC_NOTIFY_ICON;
extern const UINT WM_NOTYFYICON;

extern const int DEFAULT_CLOCK_WINDOW_WIDTH;
extern const int DEFAULT_CLOCK_WINDOW_HEIGHT;
extern const wchar_t DEFAULT_LINE_FORMAT[];
extern const uint32_t DEFAULT_TEXT_MARGIN_PIXELS;
extern const unsigned int DEFAULT_TEXT_MARGIN_CHARS;
extern const unsigned int DEFAULT_SPACE_BETWEEN_LINES;
extern const int DEFAULT_TEXT_SIZE_IN_POINTS;
extern const unsigned int DEFAULT_TAB_LENGTH_CHARS;
extern const uint8_t DEFAULT_TEXT_COLOR_R;
extern const uint8_t DEFAULT_TEXT_COLOR_G;
extern const uint8_t DEFAULT_TEXT_COLOR_B;
extern const uint8_t DEFAULT_BACKGROUND_COLOR_R;
extern const uint8_t DEFAULT_BACKGROUND_COLOR_G;
extern const uint8_t DEFAULT_BACKGROUND_COLOR_B;
extern const BOOL DEFAULT_STAY_ON_TOP;

extern const uint32_t CONFIG_FILE_VERSION;
extern const wchar_t * CONFIG_FILE_NAME;

// END OF Constants
//==============================================================================
// Variables:

extern HINSTANCE g_hInstance;

extern HWND g_hMainWindow;

extern DRAWTEXTPARAMS g_drawTextParams;

extern int g_draggingMainWindow;

extern POINT g_mousePosition;

extern HMENU g_hMainWindowPopUpMenu;
extern TPMPARAMS g_mainWindowPopUpMenuTrackParams;

extern HICON g_hApplicationIcon;
extern HICON g_hApplicationSmallIcon;

extern NOTIFYICONDATA g_notifyIconData;
extern HMENU g_hNotifyIconPopUpMenu;

extern ClockLine * g_pFirstClockLine;

extern CommonSettings g_commonSettings;

extern COLORREF g_customColors[CUSTOM_COLORS_NUMBER];

extern HBRUSH g_hBackgroundBrush;
extern HBRUSH g_hCurrentLineBrush;

extern int g_modalWindowIsOpened;

// END OF Variables
//==============================================================================
// Functions:

// mainwindowproc.c
LRESULT APIENTRY MainProc(HWND a_hWnd, UINT a_message, WPARAM a_wParam,
	LPARAM a_lParam);

// paintmainwindow.c
void paintMainWindow();

// settings.c
void saveSettings();
void loadSettings();
LRESULT APIENTRY SettingsDialogProc(HWND a_hWnd, UINT a_message,
	WPARAM a_wParam, LPARAM a_lParam);

// helpers.c
void cleanUpAndQuit();
void clearClockLines(ClockLine ** a_ppFirstLine);
void copyClockLines(ClockLine ** a_ppDestinationFirstLine,
	ClockLine * a_pSourceFirstLine);
void setClockLineToDefault(ClockLine * a_pClockLine);
void setStayOnTop(BOOL a_stayOnTop);

// formtimedatestring.c
void formTimeDateString(const wchar_t * a_format, wchar_t * a_outputString,
	unsigned int a_maxOutputStringLength);

// END OF Functions
//==============================================================================

#endif // GLOBALDEFINES_H_INCLUDED
