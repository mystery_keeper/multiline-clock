#ifndef IDC_STATIC
#define IDC_STATIC (-1)
#endif

#define MAIN_ICON                               1
#define IDD_SETTINGS_DIALOG                     101
#define IDC_LINE_FORMAT_HELP                    1001
#define IDC_OK                                  1002
#define IDC_CANCEL                              1003
#define IDC_LINES_LIST                          1007
#define IDC_ADD_LINE                            1009
#define IDC_REMOVE_LINE                         1011
#define IDC_MOVE_LINE_UP                        1014
#define IDC_MOVE_LINE_DOWN                      1015
#define IDC_CHOOSE_LINE_FONT                    1017
#define IDC_LINE_FONT_STATIC                    1018
#define IDC_CHOOSE_LINE_COLOR                   1020
#define IDC_LINE_COLOR_STATIC                   1021
#define IDC_CHOOSE_BACKGROUND_COLOR             1024
#define IDC_BACKDROUND_COLOR_STATIC             1025
#define IDC_LINE_FORMAT_STATIC                  1026
#define IDC_LINE_FORMAT_EDIT                    1028
